import 'dart:ffi';

import 'package:covidcalendar/size/size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ANLEITUNG extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: size.convertWidth(context, 20),
                    vertical: size.convert(context, 50)),
                child: Row(
                  children: [
                    DrawerFtn(),
                  ],
                ),
              ),
              Center(
                child: TExtWidget(
                  text: 'ANLEITUNG',
                  fontSize: size.convert(context, 40),
                  color: Colors.black,
                ),
              ),
              SizedBox(
                height: size.convert(context, 10),
              ),
              TExtWidget(
                text: 'Mit Überblick durch die Pandemie! ',
                fontSize: size.convert(context, 20),
                color: Color(0xFF707070),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              TExtWidget(
                text:
                    'Füge Zertifikate direkt in der App hinzu und behalte im Überblick, wann Du eines der 3-G’s erfüllst und dein Lieblingsrestaurant besuchen kannst. Hellgrüne und Dunkelgrüne Balken im Kalender sowie eine Anzeige im rechten oberen Bildschirmrand zeigen dir, wie lange deine Zertifikate noch gültig sind. Benachrichtigungen erinneren dich, wann deine Zertifikate ablaufen und helfen dir, deinen Alltag zu planen. ',
                fontSize: size.convert(context, 20),
                color: Color(0xFF707070),
              ),
              SizedBox(
                height: size.convert(context, 25),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                        print('Cross button');
                      },
                      child: SvgPicture.asset('assets/images/cross.svg')),
                ],
              ),
              SizedBox(
                height: size.convert(context, 10),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class TExtWidget extends StatelessWidget {
  final String text;
  double fontSize;
  Color color;
  TExtWidget({required this.text, required this.fontSize, required this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: GoogleFonts.quicksand(
            fontSize: fontSize, fontWeight: FontWeight.bold, color: color),
      ),
    );
  }
}

class DrawerFtn extends StatelessWidget {
  const DrawerFtn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: SvgPicture.asset(
        'assets/images/drawer.svg',
        color: Color(0xFF707070),
      ),
    );
  }
}
