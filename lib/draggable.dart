import 'package:covidcalendar/DropDown.dart';
import 'package:covidcalendar/drawer.dart';
import 'package:covidcalendar/stickyBottom.dart';
import 'package:covidcalendar/widgets/datetime.dart';
import 'package:covidcalendar/widgets/number_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class Dragablesheet extends StatefulWidget {
  const Dragablesheet({Key? key}) : super(key: key);

  @override
  _DragablesheetState createState() => _DragablesheetState();
}

class _DragablesheetState extends State<Dragablesheet> {
  var isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Wrap(children: [
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * .3,
        child: DraggableScrollableSheet(
          initialChildSize: .35,
          minChildSize: .35,
          maxChildSize: 1,
          builder: (BuildContext context, ScrollController scrollController) {
            return ListView.builder(
              controller: scrollController,
              itemCount: 1,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: MediaQuery.of(context).size.height / 1.5,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(32),
                        topRight: Radius.circular(32)),
                  ),
                  child:

                      ///DATA HERE/////
                      SingleChildScrollView(
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(18.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    // margin: EdgeInsets.only(left: 3),
                                    child: Text(
                                      'Add \nCertificate',
                                      style: GoogleFonts.quicksand(
                                          textStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 19,
                                              fontWeight: FontWeight.w800)),
                                    ),
                                  ),
                                  Container(
                                    height: 50,
                                    width: 50,
                                    child: Image.asset(
                                      'assets/bottomIcon.png',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            //////DROP DOWN////
                            DropDown(),

                            Center(
                              child: Container(
                                child: Text(
                                  'DATE',
                                  style: GoogleFonts.quicksand(
                                      textStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800)),
                                ),
                              ),
                            ),
                            Numberpicker(),
                            Container(
                                child: SvgPicture.asset(
                              "assets/dot.svg",
                              width: 10,
                              height: 10,
                            )),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: Container(
                                child: Text(
                                  'CHOOSE TIME',
                                  style: GoogleFonts.quicksand(
                                      textStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800)),
                                ),
                              ),
                            ),
                            Container(
                              child: DateTimePicker(),
                            ),
                            ButtonTheme(
                              minWidth: 150.0,
                              height: 50.0,
                              child: RaisedButton(
                                onPressed: () {},
                                color: Colors.lightGreen,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                child: Text(
                                  "SAVE",
                                  style: GoogleFonts.quicksand(
                                      textStyle: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w800)),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
      Positioned(bottom: 0, child: StickyBottom()),
    ]);
  }
}
