import 'package:covidcalendar/ANLEITUNG.dart';
import 'package:covidcalendar/events.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:table_calendar/table_calendar.dart';
import 'size/size.dart';

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  DateTime focusedDay = DateTime.now();
  DateTime _selecedDate = DateTime.now();
  Map<DateTime, List<Event>> selectedEvent = {};
  Map<DateTime, List<dynamic>> _events = {};
  List<dynamic> _selectedEvents = [];

  _getEventsForDay(DateTime day) {
    return _selectedEvents.add(day);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _events = {};
    _selectedEvents = [];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: 10),
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(
                left: size.convertWidth(context, 10),
                top: size.convert(context, 10),
                right: size.convertWidth(context, 35)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: SvgPicture.asset(
                    'assets/images/drawer.svg',
                    height: size.convert(context, 10),
                  ),
                  onTap: () {
                    DrawerBottomSheet(context);
                  },
                ),
                GreenBtn()
              ],
            ),
          ),
          TableCalendar(
            calendarBuilders: CalendarBuilders(
                todayBuilder: (context, date, event) => Container(
                  height: 55,
                  width: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.green.shade800, width: 2),
                      borderRadius: BorderRadius.circular(15)),
                  child: Text(
                    date.day.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                rangeEndBuilder: (context, date, events) => Container(
                  height: 55,
                  width: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.green.shade800,
                      borderRadius: BorderRadius.circular(15)),
                  child: Text(
                    date.day.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                selectedBuilder: (context, date, events) => GestureDetector(
                  onDoubleTap: () => Stack(children: [
                    Container(
                      height: 50,
                      width: 50,
                      color: Colors.orange,
                    ),
                  ]),
                  child: Container(
                    height: 55,
                    width: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.green.shade800,
                        border: Border.all(color: Colors.white, width: 4),
                        borderRadius: BorderRadius.circular(15)),
                    child: Text(
                      date.day.toString(),
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                )),
            rangeSelectionMode: RangeSelectionMode.disabled,
            headerStyle: HeaderStyle(
              headerMargin: EdgeInsets.only(
                  left: size.convertWidth(context, 20),
                  bottom: size.convert(context, 12),
                  top: 40

              ),
              leftChevronVisible: false,
              rightChevronVisible: false,
              formatButtonVisible: false,
              titleTextStyle: GoogleFonts.quicksand(
                  fontSize: size.convert(context, 24),
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ),
            selectedDayPredicate: (DateTime date) {
              return isSameDay(_selecedDate, date);
              print(_selecedDate);
            },
            onDaySelected: (DateTime seleceddate, DateTime focuseddate) {
              setState(() {
                focusedDay = focuseddate;
                _selecedDate = seleceddate;
              });
              print(_selecedDate);
            },
            onDayLongPressed: (DateTime selectedDay, DateTime focusedDay) {
              showDialog(
                context: context,
                builder: (ctx) => DeleteDialog(),
              );
              // showAboutDialog(
              //     context: context,
              //     applicationName: 'Do you want to delete the certificate',
              //     children: [Text('yes'), Text('no')]);
            },
            calendarFormat: CalendarFormat.month,
            rangeStartDay: _selecedDate,
            rangeEndDay: _selecedDate.add(Duration(days: 2)),
            calendarStyle: CalendarStyle(
                rangeHighlightScale: 3,
                selectedTextStyle: TextStyle(color: Colors.white),
                rangeHighlightColor: Colors.green.shade800,
                withinRangeTextStyle: TextStyle(color: Colors.white),
                rangeEndDecoration: BoxDecoration(
                    color: Colors.green.shade800, shape: BoxShape.circle),
                outsideDaysVisible: false,
                todayDecoration: BoxDecoration(
                    border: Border.all(color: Colors.green.shade800),
                    borderRadius: BorderRadius.circular(8)),
                defaultTextStyle: TextStyle(color: Colors.white)),
            firstDay: DateTime.utc(2010, 10, 16),
            lastDay: DateTime.utc(2030, 3, 14),
            focusedDay: focusedDay,
          ),
        ],
      ),
    );
  }

  Future<dynamic> DrawerBottomSheet(BuildContext context) {
    return showModalBottomSheet(
      useRootNavigator: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      ),
      context: context,
      builder: (BuildContext context) => Container(
        height: size.convert(context, 290),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50),
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: size.convert(context, 8),
            ),
            GreyContainer(),
            SizedBox(
              height: size.convert(context, 23),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (ctx) => ANLEITUNG()));
              },
              child: TextContainer(
                uperText: 'ANLEITUNG?',
                lowerText: 'BITTESCHON!',
                yes: true,
              ),
            ),
            TextContainer(
              uperText: 'GEFÄLLT\'S DIR?',
              lowerText: 'BEWERTE UNS!',
              yes: true,
            ),
            TextContainer(
              uperText: 'FEEDBACK?',
              lowerText: 'SCHICK\'S UNS!',
              yes: false,
            )
          ],
        ),
      ),
    );
  }
}

class DeleteDialog extends StatelessWidget {
  const DeleteDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      clipBehavior: Clip.antiAlias,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        height: size.convert(context, 140),
        width: size.convertWidth(context, 278.47),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(
                  vertical: size.convertWidth(context, 10)),
              padding: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 20)),
              height: size.convert(context, 60),
              width: size.convertWidth(context, 260),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.pink.shade100.withOpacity(0.4)),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'ZERTIFIKAT \nENTFERNEN?',
                    style: GoogleFonts.quicksand(fontWeight: FontWeight.w700),
                  ),
                  SvgPicture.asset('assets/delete.svg')
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'NEIN',
                    style: GoogleFonts.quicksand(
                        fontSize: size.convert(context, 20),
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: size.convert(context, 40),
                ),
                Text(
                  'JA ',
                  style: GoogleFonts.quicksand(
                      fontSize: size.convert(context, 20),
                      fontWeight: FontWeight.bold),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class GreenBtn extends StatelessWidget {
  const GreenBtn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: size.convert(context, 12)),
      alignment: Alignment.center,
      height: size.convert(context, 30),
      width: size.convertWidth(context, 90),
      decoration: BoxDecoration(
          color: Color(0xFF63C05C), borderRadius: BorderRadius.circular(10)),
      child: Text(
        '19.7-10:35',
        style: GoogleFonts.quicksand(
            color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}

class TextContainer extends StatelessWidget {
  final String uperText;
  final String lowerText;
  final bool yes;
  const TextContainer(
      {required this.uperText, required this.lowerText, required this.yes});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: size.convert(context, 5),
          ),
          Text(uperText,
              style: GoogleFonts.quicksand(
                  color: Colors.black,
                  fontSize: size.convert(context, 22),
                  fontWeight: FontWeight.bold)),
          Text(lowerText,
              style: GoogleFonts.quicksand(
                  color: Color(0xFF707070),
                  fontSize: size.convert(context, 20),
                  fontWeight: FontWeight.bold)),
          SizedBox(
            height: 2,
          ),
          yes
              ? Divider(
                  endIndent: 70,
                  indent: 70,
                  color: Colors.black,
                  thickness: 0.8,
                )
              : Container(),
        ],
      ),
    );
  }
}

class GreyContainer extends StatelessWidget {
  const GreyContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size.convert(context, 12),
      width: size.convertWidth(context, 100),
      decoration: BoxDecoration(
          color: Color(0xFF707070),
          borderRadius: BorderRadius.horizontal(
              left: Radius.circular(10), right: Radius.circular(10))),
    );
  }
}
