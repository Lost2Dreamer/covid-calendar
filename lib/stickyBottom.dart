import 'package:covidcalendar/size/size.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class StickyBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return 
        Container(
          height: 89,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [

              Container(
                child: Row(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Text("Find Testing",
                          style: GoogleFonts.quicksand(textStyle:
                          TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w800)),),


                        Text("Location",
                          style: GoogleFonts.quicksand(textStyle:
                          TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w800)),),

                      ],),
                    Container(
                        margin: EdgeInsets.only(left: 20,right: 20),
                        child: SvgPicture.asset('assets/test.svg')),
                  ],
                ),
              ),

              Container(
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        Text("Register for",
                          style: GoogleFonts.quicksand(textStyle:
                          TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w800)),),


                        Text("Vaccination",
                          style: GoogleFonts.quicksand(textStyle:
                          TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w800)),),

                      ],),
                    Container(
                        margin: EdgeInsets.only(left: 20,right: 10),
                        child: Image.asset("assets/needle.png")),
                  ],
                ),
              ),

            ],
          ),
        );
  }
}
