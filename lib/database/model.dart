class Model {
  int? id;
  String? certificatename;
  late DateTime initialdate;
  late DateTime finaldate;
  bool status = true;

  Model(this.id, this.certificatename, this.initialdate, this.finaldate,
      this.status);

  Map<String, dynamic> tomap() {
    var map = <String, dynamic>{
      'id': id,
      'certificate_name': certificatename,
      'initial_date': initialdate.toIso8601String(),
      'final_date': finaldate.toIso8601String,
      'status': status ? '1' : '0'
    };
    return map;
  }

  Model.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    certificatename = map['certificate_name'];
    initialdate = map['initial_date'];
    finaldate = map['final_date'];
    status = map['status'];
  }
}
