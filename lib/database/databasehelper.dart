import 'dart:async';
import 'dart:io' as io;
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'model.dart';

class DBhelper {
  static Database? _db;
  static const ID = 'id';
  static const certificatename = 'certificate_name';
  static const initialdate = 'initial_date';
  static const finaldate = 'final_date';
  static const status = 'status';

  static const String Tablename = 'certificatetable';

  static const String dbname = 'covidcalender.db';

  Future<dynamic> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initdb();
    return _db;
  }

  initdb() async {
    io.Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, dbname);
    var db = await openDatabase(path, version: 1, onCreate: _oncreate);
    return db;
  }

  _oncreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $Tablename($ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , $certificatename TEXT , $initialdate TEXT , $finaldate TEXT , $status TEXT)');
  }

  Future<Model> insertdata(Model model) async {
    var dbclient = await db;
    model.id = await dbclient.insert(Tablename, model.tomap());

    return model;
  }

  Future<List<Model>> getdata() async {
    var dbclient = await db;

    final result = await dbclient.query(Tablename);
    return result.map((json) => Model.fromMap(json)).toList();
  }

  Future<int> delete(int id) async {
    var dbclient = await db;
    return await dbclient.delete(Tablename, where: '$ID=?', whereArgs: [id]);
  }

  Future close() async {
    var dbclient = await db;
    dbclient.close();
  }
}
