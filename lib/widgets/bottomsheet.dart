import 'package:covidcalendar/stickyBottom.dart';
import 'package:covidcalendar/widgets/datetime.dart';
import 'package:covidcalendar/widgets/datetime.dart';
import 'package:covidcalendar/widgets/drop_down.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:numberpicker/numberpicker.dart';

import 'number_picker.dart';

class Bottomsheet extends StatefulWidget {
  @override
  _BottomsheetState createState() => _BottomsheetState();
}

class _BottomsheetState extends State<Bottomsheet> {
  var _chosenValue;
  int _currentValue=6;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Wrap(
        // alignment: WrapAlignment.center,
        children: [
          InkWell(
            child: GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    context: context,
                    builder: (ctx) {
                      return Container(
                        //height: 550,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(32),
                              topRight: Radius.circular(32)),
                        ),
                        child:

                            ///DATA HERE/////
                      Center(
                          child: Padding(
                            padding: const EdgeInsets.all(18.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 30, vertical: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        // margin: EdgeInsets.only(left: 3),
                                        child: Text(
                                          'Add \nCertificate',
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        height: 50,
                                        width: 50,
                                        child: Image.asset(
                                          'assets/bottomIcon.png',
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                //////DROP DOWN////
                                Center(
                                  child: Container(
                                    width: MediaQuery.of(context).size.width - 85,
                                    decoration: ShapeDecoration(
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            width: 1.5, style: BorderStyle.solid),
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(23)),
                                      ),
                                    ),
                                    padding: const EdgeInsets.all(2.0),
                                    child: DropDown(),


                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Center(
                                  child: Container(
                                    child: Text(
                                      'DATE',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                    ),
                                  ),
                                ),
                                Numberpicker(),

                                Container(

                                  child: SvgPicture.asset("assets/dot.svg", width: 10, height: 10,)
                                ),

                                Center(
                                  child: Container(
                                    child: Text(
                                      'CHOOSE TIMER',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: DateTimePicker(),
                                ),
                                ButtonTheme(
                                  minWidth: 140.0,
                                  height: 50.0,
                                  child: RaisedButton(
                                    onPressed: () {},
                                    color: Colors.lightGreen,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Text(
                                      "SAVE",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32), topRight: Radius.circular(32)),
                ),
                width: double.infinity,
                height: MediaQuery.of(context).size.height / 8,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        // margin: EdgeInsets.only(left: 3),
                        child: Text(
                          'Add \nCertificate',
                          style:
                              TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                        ),
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        child: Image.asset(
                          'assets/bottomIcon.png',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          StickyBottom(),
        ],
      ),
    );
  }
}
