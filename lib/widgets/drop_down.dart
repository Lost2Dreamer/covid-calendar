
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DropDown extends StatefulWidget {
  @override
  DropDownWidget createState() => DropDownWidget();
}

class DropDownWidget extends State {

  String dropdownValue = 'ART DES ZERTIFIKATS';

  List <String> spinnerItems = [
    'ART DES ZERTIFIKATS',
    'Two',
    'Three',
    'Four',
    'Five'
  ] ;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DropdownButton<String>(
              value: dropdownValue,
              iconSize: 24,
              elevation: 16,
              style: GoogleFonts.quicksand(textStyle:
              TextStyle(color: Colors.black, fontSize: 35, fontWeight: FontWeight.w500)),
              onChanged: (data) {
                setState(() {
                  dropdownValue = data.toString();
                });
              },
              items: spinnerItems.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
    );
  }
}
