import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';

class Numberpicker extends StatefulWidget {


  @override
  _NUmberPickerState createState() => _NUmberPickerState();
}

class _NUmberPickerState extends State<Numberpicker> {
  int _currentIntValue = 10;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: NumberPicker(
        axis: Axis.horizontal,
        selectedTextStyle: TextStyle(color: Colors.black, fontSize: 40),
        minValue: 1,
        maxValue: 31,
        step: 1,

        onChanged: (value) {
          setState(() {
            _currentIntValue = value;
            print(value);
          });
        },
        value: _currentIntValue,
      ),
    );
  }
}
